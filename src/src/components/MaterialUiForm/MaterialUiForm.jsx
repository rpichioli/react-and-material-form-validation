import React from 'react';
import 'date-fns';
import { Fab, ButtonGroup, IconButton, FormGroup, FormLabel, FormControl, FormHelperText, FormControlLabel, Input, InputLabel, Grid, Typography, Checkbox, Button, Icon, Snackbar, SnackbarContent} from '@material-ui/core';
// import { Delete as DeleteIcon, CloudUpload as CloudUploadIcon, KeyboardVoice as KeyboardVoiceIcon, Save as SaveIcon } from '@material-ui/icons';
import {CheckCircle as CheckCircleIcon, Error as ErrorIcon, Close as CloseIcon} from '@material-ui/icons';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

/**
 * @desc MaterialUiForm is the form validation component itself
 */
class MaterialUiForm extends React.Component {

	state = {
		firstName: '',
		lastName: '',
		additionalDetails: '',
		terms: true,
		newsletter: true,
		creationDate: new Date(),
		creationTime: new Date(),
		successful: false,
		invalid: false,
		errors: {}
	}

	clearForm = () => this.setState({ firstName: '', lastName: '', additionalDetails: '', terms: false, newsletter: false, errors: {} });

	validateForm = () => {
		const {firstName, lastName, terms} = this.state;
		let errors = {};
		if (!firstName) errors.firstName = 'First name is required';
		if (!lastName) errors.lastName = 'Last name is required';
		if (!terms) errors.terms = 'Terms must be accepted';
		return { errors, isValid: Object.keys(errors).length === 0 };
	}

	handleChange = (e) => this.setState({ [e.target.name]: e.target.value });
	handleCheckbox = (e) => this.setState({ [e.target.name]: e.target.checked });
	handleSubmit = (e) => {
		e.preventDefault();
		let { errors, isValid } = this.validateForm();
		if (!isValid) this.setState({ errors, invalid: true });
		else {
			this.clearForm();
			this.setState({ successful: true });
		}
	}

  render() {
		// const classes = useStyles1();
    return (
			<React.Fragment>
				<MuiPickersUtilsProvider utils={DateFnsUtils}>
					<Typography color="text.primary" variant="h4" style={{borderBottom: '1px dashed #999', paddingBottom: '8px', marginBottom: '8px'}}>Form Management</Typography>
					<Typography color="text.secondary" variant="h6">Behaviour and validation within React stateful scope</Typography>
					<br />
					<form noValidate onSubmit={this.handleSubmit}>
						<Grid container spacing={3}>
							<Grid item xs={12} sm={6}>
								<FormControl fullWidth error={this.state.errors.firstName} required>
									<InputLabel htmlFor="firstName">First name</InputLabel>
									<Input placeholder='Example: Antonio' id="firstName" name="firstName" value={this.state.firstName} onChange={this.handleChange} aria-describedby="firstName-text" />
									<FormHelperText id="firstName-text">{this.state.errors.firstName}</FormHelperText>
								</FormControl>
							</Grid>
							<Grid item xs={12} sm={6}>
								<FormControl fullWidth error={this.state.errors.lastName} required>
									<InputLabel htmlFor="lastName">Last name</InputLabel>
									<Input placeholder='Example: Banderas' id="lastName" name="lastName" value={this.state.lastName} onChange={this.handleChange} aria-describedby="lastName-text" />
									<FormHelperText id="lastName-text">{this.state.errors.lastName || 'Your complete middle and last name'}</FormHelperText>
								</FormControl>
							</Grid>
							<Grid item xs={12} sm={12}>
								<FormControl fullWidth error={this.state.errors.additionalDetails}>
									<InputLabel htmlFor="additionalDetails">Additional details</InputLabel>
									<Input multiline rows={3} placeholder='Maximum of 500 characters' id="additionalDetails" name="additionalDetails" value={this.state.additionalDetails} onChange={this.handleChange} aria-describedby="additionalDetails-text" />
									<FormHelperText id="additionalDetails-text">{this.state.errors.additionalDetails || 'Provide any details or information you think is valuable to say'}</FormHelperText>
								</FormControl>
							</Grid>
							<Grid item xs={12} sm={3}>
								<KeyboardDatePicker
									id="creationDate"
									label="Creation date"
									value={this.state.creationDate}
									onChange={this.handleChange}
									KeyboardButtonProps={{ 'aria-label': 'change date', }}
								/>
							</Grid>
							<Grid item xs={12} sm={3}>
								<KeyboardTimePicker
									id="creationTime"
									label="Creation time"
									value={this.state.creationTime}
									onChange={this.handleChange}
									KeyboardButtonProps={{ 'aria-label': 'change time', }}
								/>
							</Grid>
							<Grid item xs={12} sm={6}>
								<FormControl fullWidth error={this.state.errors.hobby}>
									<InputLabel htmlFor="hobby">Hobby</InputLabel>
									<Input placeholder='Example: Guitar, soccer, dancing' id="hobby" name="hobby" value={this.state.hobby} onChange={this.handleChange} aria-describedby="hobby-text" />
									<FormHelperText id="hobby-text">What do you love to do?</FormHelperText>
								</FormControl>
							</Grid>
							<Grid item xs={12}>
								<br />
								<FormControl component="fieldset" error={this.state.errors.terms}>
									<FormLabel component="legend">Complementary</FormLabel>
									<FormGroup>
										<FormControlLabel control={<Checkbox name="terms" color="primary" checked={this.state.terms} onChange={this.handleCheckbox} />} label="I agree and accept the purposed terms *" />
										<FormControlLabel control={<Checkbox name="newsletter" checked={this.state.newsletter} onChange={this.handleCheckbox} />} label="I want to receive the daily newsletter" />
									</FormGroup>
									<FormHelperText>{this.state.errors.terms || 'You must accept the terms to proceed'}</FormHelperText>
								</FormControl>
							</Grid>
						</Grid>

						<br />
						<br />

						<ButtonGroup
							fullWidth
							color="primary"
							variant="contained"
						>
							<Button variant="contained" color="secondary" onClick={this.clearForm}>Clear&nbsp;<Icon>clear_all</Icon></Button>
							<Button type="submit">Send&nbsp;<Icon>send</Icon></Button>
						</ButtonGroup>
					</form>

					<Snackbar
						anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
						open={this.state.invalid}
						autoHideDuration={6000}
						onClose={() => this.setState({ invalid: false })}
					>
						<SnackbarContent
							style={{backgroundColor: 'red'}}
							aria-describedby="client-snackbar"
							message={
								<span id="client-snackbar" style={{display: 'flex', alignItems: 'center'}}>
									<Icon style={{fontSize: 20, opacity: 0.9}} />
									Ops, something wrong happens!
								</span>
							}
							action={[
								<IconButton key="close" aria-label="Close" color="inherit" onClick={() => this.setState({ invalid: false })}>
									<CloseIcon style={{fontSize: 20}} />
								</IconButton>,
							]}
						/>
					</Snackbar>

					<Snackbar
						anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
						open={this.state.successful}
						autoHideDuration={6000}
						onClose={() => this.setState({ successful: false })}
					>
						<SnackbarContent
							style={{backgroundColor: 'green'}}
							aria-describedby="client-snackbar"
							message={
								<span id="client-snackbar" style={{display: 'flex', alignItems: 'center'}}>
									<Icon style={{fontSize: 20, opacity: 0.9}} /> Validation passed successfully
								</span>
							}
							action={[
								<IconButton key="close" aria-label="Close" color="inherit" onClick={() => this.setState({ successful: false })}>
									<CloseIcon style={{fontSize: 20}} />
								</IconButton>,
							]}
						/>
					</Snackbar>
				</MuiPickersUtilsProvider>
	    </React.Fragment>
    );
  }
}

export default MaterialUiForm;
