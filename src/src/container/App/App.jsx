import React from 'react';
import './App.css';
import MaterialUiForm from '../../components/MaterialUiForm/MaterialUiForm.jsx';

import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import { useTheme, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { red, pink, purple, deepPurple, indigo, blue, lightBlue, cyan, teal, green, lightGreen, lime, yellow, amber, orange, deepOrange } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
	appBar: { position: 'relative', marginBottom: '15px' },
	layout: {
		width: 'auto', marginLeft: theme.spacing(2), marginRight: theme.spacing(2),
		[theme.breakpoints.up(600 + theme.spacing(2) * 2)]: { width: 700, marginLeft: 'auto', marginRight: 'auto' }
	}
}));

const theme = createMuiTheme({
	palette: {
    type: 'dark',
    primary: lightBlue,
    secondary: cyan,
		red: red,
		contrastThreshold: 1,
		tonalOffset: 0.2
  },
  // status: {
  //   danger: 'orange',
  // },
});

function App() {
	const classes = useStyles();
  return (
    <ThemeProvider theme={theme}>
			<CssBaseline />
      <AppBar position="absolute" color="default" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>React and Material UI</Typography>
        </Toolbar>
      </AppBar>
			<main className={classes.layout}>
				<MaterialUiForm />
			</main>
		</ThemeProvider>
  );
}

export default App;
